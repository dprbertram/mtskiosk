from django.conf.urls import url
from login.views import Index

app_name='login'
urlpatterns = [
    url(r'^$', Index.as_view(), name='user_login'),
]