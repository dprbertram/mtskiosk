import ldap3
from ldap3 import NTLM, ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES
import re


class Authenticate:
    def auth(username, password):
        LDAPURI = "ldap://mtstrans.com"
        LDAPBASE = "DC=mtstrans,DC=com"
        BINDDN = "mtstrans\\{0}".format(username)
        BINDPW = password
        userid = str(username).upper()
        FILTER = "(sAMAccountName={0})".format(userid)
        ATTRIBUTES = [ALL_ATTRIBUTES, ALL_OPERATIONAL_ATTRIBUTES]

        server = ldap3.Server(LDAPURI, use_ssl=False, connect_timeout=5)

        ldapConnection = ldap3.Connection(server,
                                          user=BINDDN,
                                          password=BINDPW,
                                          authentication=NTLM,
                                          raise_exceptions=False)

        ldapConnection.open()
        authentic = ldapConnection.bind()
        if not authentic:
            return False



        ldapConnection.search(search_base=LDAPBASE,
                              search_scope=ldap3.SUBTREE,
                              search_filter=FILTER,
                              attributes=ATTRIBUTES)

        response = ldapConnection.response
        phoneSiteId = ''

        attributesDict = response[0]
        attr = attributesDict.get('attributes')
        name = attr.get('cn')
        membership = attr.get('memberOf')
        validMember = str(membership).upper().find("MTS_local_admins".upper())
        ValidMembership = False
        if validMember > -1:
            ValidMembership = True

        if not ValidMembership:
            return False

        DataContext = {'name':name[0] }

        return DataContext
