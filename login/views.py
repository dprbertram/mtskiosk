from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.views import View

from login.Authenicate.authenticate import Authenticate
from .loginform import LoginForm

class Index(View):
    def get(self, request):
        success = True
        form = LoginForm()
        context = {'form': form, 'success': success}
        return render(request, 'login.html', context)


    def post(self, request):
        form = LoginForm()
        userId = request.POST['user_name']
        password = request.POST['password']
        if password != "kio$k":
            authenticated = Authenticate.auth(userId, password)
        else:
           return redirect('terminal:index')
        if not authenticated:
            success = False
            context = {'form': form, 'success': success}
            request.session['loggedIn'] = False
            return redirect('terminal:index')
        else:
            request.session['loggedIn'] = True
            request.session['UserData'] = authenticated
            return redirect('contentmanager:index')
