from django.shortcuts import render, redirect
from django.views.generic import ListView
from fm.views import AjaxCreateView, AjaxUpdateView, AjaxDeleteView
from django.views.generic import ListView, CreateView
from django.http import HttpResponseRedirect
from django.views import View
from .models import links
from .forms import add_link

class Index(View):
    def get(self, request):
        return redirect("contentmanager:urllist")

class UrlList(ListView):
    model = links
    template_name = 'url_list.html'

class LinkCreateView(AjaxCreateView):
    form_class = add_link

class LinkEditView(AjaxUpdateView):
    form_class = add_link
    model = links
    pk_url_kwarg = 'url_pk'

class LinkDeleteView(AjaxDeleteView):
     model = links
     pk_url_kwarg = 'url_pk'
