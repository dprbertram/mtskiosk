from django.forms import ModelForm
from .models import links
from django.utils.timezone import datetime
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, ButtonHolder, Submit, HTML
from crispy_forms.bootstrap import FormActions


class add_link(ModelForm):
    class Meta:
        model = links
        fields = ['name', 'url']
