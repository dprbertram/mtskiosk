from django.conf.urls import url
from contentmanager.views import Index, UrlList, LinkCreateView, LinkEditView, LinkDeleteView

app_name = "contentmanager"
urlpatterns = [
    url(r'^$', Index.as_view(), name="index"),
    url(r'^urllist/$', UrlList.as_view(), name="urllist"),
    url(r'^create/$', LinkCreateView.as_view(), name="link_create"),
    url(r'^update/(?P<url_pk>\d+)/$', LinkEditView.as_view(), name="link_update"),
    url(r'^delete/(?P<url_pk>\d+)/$', LinkDeleteView.as_view(), name="link_delete")


]