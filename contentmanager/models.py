from django.db import models
from django.utils import timezone

class links(models.Model):
    name = models.CharField('name', max_length=255, blank=False)
    url = models.URLField('url', max_length=255, blank=False)
    created = models.DateTimeField('created',default=timezone.now())