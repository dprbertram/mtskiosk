from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from contentmanager.models import links
from django.views.generic import ListView

class Index(View):
    def get(self, request):
        linkList = links.objects.all()
        dataContext = {'object_list': linkList}
        return render(request, 'index.html', dataContext)


class Welcome(View):
    def get(self, request):
        return render(request, 'welcome.html')

