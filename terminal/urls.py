from django.conf.urls import url
from terminal.views import Index, Welcome

app_name = "terminal"
urlpatterns = [
    url(r'^$', Index.as_view(), name="index"),
    url(r'^welcome/$', Welcome.as_view(), name="welcome")

]